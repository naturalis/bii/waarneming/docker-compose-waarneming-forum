---
services:
  traefik:
    image: registry.gitlab.com/naturalis/lib/docker/traefik:${TRAEFIK_VERSION:?err}
    container_name: "${COMPOSE_PROJECT_NAME:-composeproject}_traefik"
    restart: ${CONTAINER_RESTART:-unless-stopped}
    depends_on:
      - docker-socket-proxy
      - smf
    networks:
      - web
      - docker-socket-proxy
    ports:
      - "${HOST_IP:-0.0.0.0}:80:80"
      - "${HOST_IP:-0.0.0.0}:443:443"
      - "127.0.0.1:${TRAEFIK_API_PORT:-8079}:8080"
    volumes:
      - "./letsencrypt:/letsencrypt"
      - "./traefik/traefik-dynamic-config.toml:/traefik-dynamic-config.toml"
    command:
      - --global.sendAnonymousUsage=false
      - --log.level=${TRAEFIK_LOG_LEVEL:-ERROR}
      - --api=${TRAEFIK_API_ENABLED:-false}
      - --api.insecure=true
      - --entrypoints.web.address=:80
      - --entrypoints.websecure.address=:443
      - --certificatesresolvers.route53.acme.dnschallenge=true
      - --certificatesresolvers.route53.acme.dnschallenge.provider=route53
      - --certificatesresolvers.route53.acme.storage=/letsencrypt/route53.json
      - --providers.docker=true
      - --providers.docker.exposedbydefault=false
      - --providers.docker.endpoint=tcp://docker-socket-proxy:2375
      - --providers.docker.network=docker-socket-proxy
    env_file:
      - .env

  docker-socket-proxy:
    image: tecnativa/docker-socket-proxy:${DOCKERPROXY_VERSION:-latest}
    container_name: "${COMPOSE_PROJECT_NAME:-composeproject}_docker-socket-proxy"
    restart: unless-stopped
    networks:
      - docker-socket-proxy
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock:ro,delegated
    environment:
      CONTAINERS: 1

  smf:
    # image: registry.gitlab.com/naturalis/bii/waarneming/docker-waarneming-forum:${SMF_VERSION:-v2-0-17}
    # image: registry.gitlab.com/naturalis/bii/waarneming/docker-waarneming-forum/v2-1-2:7a761d8ae5d74449f18116f815724aae808e767b
    image: registry.gitlab.com/naturalis/bii/waarneming/docker-waarneming-forum/simplemachines-forum:latest
    restart: unless-stopped
    container_name: "${COMPOSE_PROJECT_NAME:-composeproject}_smf"
    depends_on:
      - db
    env_file:
      - .env
    ports:
      - ${WEB_EXTERNAL_PORT:-8080}:80
    networks:
      - web
      - backend
    labels:
      - traefik.enable=true
      - traefik.docker.network=${COMPOSE_PROJECT_NAME:-composeproject}_web
      #      - traefik.http.services.smf.loadbalancer.server.port=80
      - traefik.http.routers.redirect-http.entrypoints=web
      - traefik.http.routers.redirect-http.rule=${NGINX_FQDN:-Host(`replace.me.dryrun.link`)}
      - traefik.http.routers.redirect-http.middlewares=redirect-to-https
      - traefik.http.middlewares.redirect-to-https.redirectscheme.scheme=https
      - traefik.http.middlewares.redirect-to-https.redirectscheme.permanent=true
      - traefik.http.routers.smf.tls.certresolver=route53
      - traefik.http.routers.smf.tls=true
      - traefik.http.routers.smf.rule=${SMF_SITE_URL:-Host(`smf-test.naturalis.io`)}
      - traefik.http.routers.smf.middlewares=smf-stripprefix@docker
      - traefik.http.middlewares.smf-stripprefix.stripprefix.prefixes=/smf
      # hsts
      - traefik.http.routers.nginx.middlewares=hsts
      - traefik.http.middlewares.hsts.headers.stsSeconds=31536000
      - traefik.http.middlewares.hsts.headers.stsIncludeSubdomains=true
      - traefik.http.middlewares.hsts.headers.stsPreload=true
      - traefik.http.middlewares.hsts.headers.forceSTSHeader=true

    volumes:
      - ${BASE_PATH:-/data}/attachments/attachments:/data/attachments
      - ${BASE_PATH:-/data}/www:/var/www/html

  db:
    image: "mariadb:10.4"
    restart: unless-stopped
    container_name: "${COMPOSE_PROJECT_NAME:-composeproject}_db"
    environment:
      MYSQL_ROOT_PASSWORD: ${MYSQL_ROOT_PASSWORD:-smftest}
      MYSQL_DATABASE: ${MYSQL_DATABASE:-smf}
      MYSQL_USER: ${MYSQL_USER:-smf}
      MYSQL_PASSWORD: ${MYSQL_PASSWORD:-test123}
    networks:
      - backend
    volumes:
      - ${BASE_PATH:-/data}/database/db:/var/lib/mysql

  smtp:
    image: namshi/smtp
    restart: always
    container_name: "${COMPOSE_PROJECT_NAME:-composeproject}_smtp"
    networks:
      - web
    environment:
      - MAILNAME=${MAIL_NAME:-mail.forum.example.com}

# Networks
networks:
  web:
    internal: false
  backend:
    internal: true
  docker-socket-proxy:
    internal: true
