docker-compose-waarneming-forum
====================

dockercompose file for simplemachines forum. 
- (custom) php-apache container with SMF install (https://gitlab.com/naturalis/bii/waarneming/docker-waarneming-forum)
- mariadb database container
- smtp container for sending email
- traefik container for reverse proxy / ssl offloading and for rewriting /smf url's



Contents
-------------
- dockercompose.yml
- env.template

General
-------------
Simplemachines forum 


Run container
-------------

```
docker-compose up -d
```





